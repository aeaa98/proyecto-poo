import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Connection {
    static SessionFactory studentFactory;
    static Session studentSession;
    static SessionFactory patientFactory;
    static Session patientSession;
    static SessionFactory medicineFactory;
    static Session medicineSession;
    static SessionFactory universityFactory;
    static Session univeristySession;
    public Connection() {
        studentFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
        studentSession  = studentFactory.getCurrentSession();
        studentSession.beginTransaction();

        patientFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Patient.class).buildSessionFactory();
        patientSession  = patientFactory.getCurrentSession();
        patientSession.beginTransaction();

        medicineFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Medicine.class).buildSessionFactory();
        medicineSession  = medicineFactory.getCurrentSession();
        medicineSession.beginTransaction();

        universityFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(University.class).buildSessionFactory();
        univeristySession  = universityFactory.getCurrentSession();
        univeristySession.beginTransaction();
    }
}
