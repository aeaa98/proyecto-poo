import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="appointments")
public class Appointment {
    @Id
    @Column(name="id")
    private int id;

    @Column(name="observations")
    private String obervations;


    @Column(name="patients_id")
    private int patientId;

    @Column(name="student_id")
    private String studentId;

    @Column(name="appointment_type_id")
    private int appointmentTypeId;

    public Appointment(String obervations, int patientId, String studentId, int appointmentTypeId) {
        this.obervations = obervations;
        this.patientId = patientId;
        this.studentId = studentId;
        this.appointmentTypeId = appointmentTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getObervations() {
        return obervations;
    }

    public void setObervations(String obervations) {
        this.obervations = obervations;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public int getAppointmentTypeId() {
        return appointmentTypeId;
    }

    public void setAppointmentTypeId(int appointmentTypeId) {
        this.appointmentTypeId = appointmentTypeId;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", obervations='" + obervations + '\'' +
                ", patientId=" + patientId +
                ", studentId='" + studentId + '\'' +
                ", appointmentTypeId=" + appointmentTypeId +
                '}';
    }
}
